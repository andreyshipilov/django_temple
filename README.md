## What? ##

This is a blank *Django 1.11.x* template which is used for rapid project initialization. 

## How? ##

First of all you need:

* Python (the interpreter) — https://www.python.org/downloads/
* Python Setuptools (initial package installer) — https://pypi.python.org/pypi/setuptools
* PIP (advanced package installer) — https://pypi.python.org/pypi/pip

Install Python and setup tools. These are installed using standard installers. After that install PIP from the command line.
```
easy_install pip
```

Install isolated virtual environment package.
```
pip install -U virtualenv
```

Make sure that virtualenv has the latest version — global packages will be not in stalled automatically. Create a new virtual environment.
```
virtualenv venv
```

Activate the new environment.
```
// *nix
. ./venv/bin/activate

// Windows 
. ./venv/scripts/activate
```

Install Django version less than 2.
```
pip install 'django<2'
```

Start a Django project passing extra options to "django-admin.py" replacing "**new_project**" with an actual project name.
```
django-admin.py startproject --template=https://bitbucket.org/andreyshipilov/django_temple/get/master.zip new_project
```

Rename the created directory appending "**_django**" to the end. Initialize the GIT repository inside the *_django* directory.
```
mv new_project new_project_django
cd new_project_django
git init
```

## Apache 2 config ##

Is automatically created and named correspondingly for staging and production environment. Remove the **.py** extension and use it as normal.

## Docs ##

https://docs.djangoproject.com/en/1.10/ref/django-admin/#startproject-projectname-destination

## PS ##

And thanks for all the fish.