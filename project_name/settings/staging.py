"""
Staging settings.
"""

from base import *


DEBUG = True

ALLOWED_HOSTS = ['*']

WSGI_APPLICATION = '{{ project_name }}.wsgi.staging.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '{{ project_name }}_staging',
        'USER': '{{ project_name }}_staging',
        'PASSWORD': '',
    }
}

COMPRESS_ENABLED = True

COMPRESS_CSS_FILTERS = [
    'compressor.filters.template.TemplateFilter',
    'compressor.filters.css_default.CssAbsoluteFilter',
    'compressor.filters.cssmin.CSSMinFilter',
]

COMPRESS_JS_FILTERS = [
    'compressor.filters.template.TemplateFilter',
    'compressor.filters.jsmin.JSMinFilter',
]
